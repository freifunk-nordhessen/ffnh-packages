# ffnh-tunneldigger-fix
Dieses Paket soll die folgenden Probleme in der Firmware Gluon v2023.1 fixen:
- DNS Auflösung im Tunneldigger Dienst
- 5 minütigen Neustart des Tunneldiggers aufgrund der fehldenden PID Datei